﻿using System;

namespace RemoveSubmissionBot
{
    public class CPrint
    {
        public static void PrintLine(string text, MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.Generic:
                    Console.WriteLine(string.Format("{0}", text));
                    return;
                case MessageType.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                case MessageType.Warning:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                case MessageType.Success:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                case MessageType.Information:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                default:
                    return;
            }
        }
        public static void Print(string text, MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.Generic:
                    Console.Write(string.Format("{0}", text));
                    return;
                case MessageType.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                case MessageType.Warning:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                case MessageType.Success:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                case MessageType.Information:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(string.Format("{0}", text));
                    Console.ResetColor();
                    return;
                default:
                    return;
            }
        }
        public enum MessageType
        {
            Generic,
            Error,
            Warning,
            Success,
            Information
        }

    }
}
