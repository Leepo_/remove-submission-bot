﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using RedditSharp;
using RedditSharp.Things;

namespace RemoveSubmissionBot
{
    public class Program
    {
        /*If you wish to change the number of hours of which a post will be 
        deleted after or the score of the post change the two variables below to what you want.*/
        private static void Main(string[] args)
        {
            Reddit reddit = null;
            Subreddit subreddit;
            DateTime now;
            var authenticated = false;
            int score = 0;
            int time = 3;
            int count = 0;

            Console.Title = "Remove Submission Bot               Submissions Removed: " + count;
            CPrint.Print("Enter Username: ",CPrint.MessageType.Generic);
            var username = Console.ReadLine();
            CPrint.Print("Enter Password: ", CPrint.MessageType.Generic);
            var password = ReadPassword();
            try
            {
                CPrint.Print("Logging in...\n", CPrint.MessageType.Information);
                reddit = new Reddit(username, password);
                authenticated = reddit.User != null;
                if (authenticated)
                    CPrint.PrintLine("We logged in!", CPrint.MessageType.Success);
            }
            catch (AuthenticationException)
            {
                CPrint.PrintLine("Unable to log in. Press anykey to close.", CPrint.MessageType.Error);
                authenticated = false;
                Console.ReadKey();
                Environment.Exit(0);
            }
            CPrint.Print("Enter Subreddit to watch for submissions: ", CPrint.MessageType.Generic);
            subreddit = reddit.GetSubreddit("/r/" + Console.ReadLine());
            CPrint.PrintLine(string.Format("Currently watching {0} for submmisions that have a score less than {1}, and are older than {2} hours old.", subreddit, score,time), CPrint.MessageType.Information);
            if (!subreddit.UserIsModerator.Value)
            {
                CPrint.PrintLine("This user account is not a moderator of the current sub being watched! Program has stopped press any key to close.", CPrint.MessageType.Error);
                Console.ReadKey();
                Environment.Exit(0);
            }
            while (true)
            {
                
                foreach (var post in subreddit.Posts.Take(25))
                {
                    if (CheckPostTime(post.CreatedUTC) && post.Score <= score)
                    {
                        count++;
                        CPrint.PrintLine("Deleting the post: " + post.Title, CPrint.MessageType.Information);
                        try
                        {
                            post.Remove();
                            CPrint.PrintLine("Post has been deleted.",CPrint.MessageType.Success);
                            Console.Title = "Remove Submission Bot               Submissions Removed: " + count;
                        }
                        catch
                        {
                        }


                    }

                }
            }
        }

        private static bool CheckPostTime(DateTime time)
        {
            DateTime now = DateTime.UtcNow;
            TimeSpan timeDiffrence = time - now;
            double hours = timeDiffrence.TotalHours;
            if (hours < 0)
            {
                timeDiffrence = now - time;
                hours = timeDiffrence.TotalHours;
            }
            if (hours >= 0)
                return true;
            return false;
        }
        private static string ReadPassword()
        {
            var passbits = new Stack<string>();
            //keep reading
            for (ConsoleKeyInfo cki = Console.ReadKey(true); cki.Key != ConsoleKey.Enter; cki = Console.ReadKey(true))
            {
                if (cki.Key == ConsoleKey.Backspace)
                {
                    if (passbits.Count() > 0)
                    {
                        //rollback the cursor and write a space so it looks backspaced to the user
                        Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                        Console.Write(" ");
                        Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                        passbits.Pop();
                    }
                }
                else
                {
                    Console.Write("*");
                    passbits.Push(cki.KeyChar.ToString());
                }
            }
            string[] pass = passbits.ToArray();
            Array.Reverse(pass);
            Console.Write(Environment.NewLine);
            return string.Join(string.Empty, pass);
        }
    }
}
